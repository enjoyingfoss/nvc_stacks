// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/emotion_chip.dart';
import 'package:empado/components/perspective_statement/base_perspective_statement.dart';
import 'package:empado/models/list_perspective.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HistoryPerspectiveStatement extends StatelessWidget {
  final ListPerspective listPerspective;

  const HistoryPerspectiveStatement({required this.listPerspective, super.key});

  @override
  Widget build(BuildContext context) {
    final perspective = listPerspective.perspective;
    final person = listPerspective.person;

    final textStyle = Theme.of(context).textTheme.bodyMedium;

    return BasePerspectiveStatement(
      textStyle: textStyle,
      personName: person.name,
      personPronoun: person.pronoun,
      isOwn: perspective.ownDescription,
      emotionSpans: listPerspective.emotions.map((pe) => WidgetSpan(
          alignment: PlaceholderAlignment.middle,
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
              child: EmotionChip(emotion: pe.emotion, textStyle: textStyle)))),
      needStatementSpan: TextSpan(
          text: TemporaryPerson.personIsYou(person)
              ? perspective.isAppreciation
                  ? AppLocalizations.of(context).needStatementMeetingNeedsForMe
                  : AppLocalizations.of(context).needStatementNeedingI
              : perspective.isAppreciation
                  ? perspective.ownDescription
                      ? AppLocalizations.of(context)
                          .needStatementMeetingOwnNeedsForThem(
                              person.pronoun.localeKey)
                      : AppLocalizations.of(context)
                          .needStatementMeetingAssumedNeedsForThem(
                              person.pronoun.localeKey)
                  : perspective.ownDescription
                      ? AppLocalizations.of(context)
                          .needStatementNeedingOwnThey(person.pronoun.localeKey)
                      : AppLocalizations.of(context)
                          .needStatementNeedingAssumedThey(
                              person.pronoun.localeKey),
          style: textStyle),
      needSpans: listPerspective.needs.map((pn) => WidgetSpan(
          alignment: PlaceholderAlignment.middle,
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
              child: Chip(
                  label: Text(pn.need.getTranslation(context),
                      style: textStyle))))),
    );
  }
}
