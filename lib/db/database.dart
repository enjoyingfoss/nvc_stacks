// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:empado/enums/emotion.dart';
import 'package:empado/enums/need.dart';
import 'package:empado/enums/need_category.dart';
import 'package:empado/enums/pronoun.dart';
import 'package:empado/models/editable_perspective.dart';
import 'package:empado/models/editable_situation.dart';
import 'package:empado/models/list_perspective.dart';
import 'package:empado/models/list_situation.dart';
import 'package:empado/models/list_strategy.dart';
import 'package:empado/models/temporary_person.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

/// Run `flutter pub run build_runner build --delete-conflicting-outputs`

part 'database.g.dart';

@DataClassName("Person")
class People extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text().nullable()(); // null = you
  IntColumn get pronoun => intEnum<Pronoun>()();
}

//TODO add a rudimentary todo / habit list (not sure yet if in the same UI or on different screens)

@DataClassName("Strategy")
class Strategies extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get strategy => text()();
  BoolColumn get archived => boolean().withDefault(const Constant(false))();
  BoolColumn get bookmarked => boolean().withDefault(const Constant(false))();
  DateTimeColumn get lastUsed => dateTime()();
}

class StrategyNeeds extends Table {
  IntColumn get strategy => integer().references(Strategies, #id)();
  TextColumn get need => textEnum<Need>()();
  TextColumn get cachedNeedCategory => textEnum<NeedCategory>()();

  @override
  Set<Column>? get primaryKey => {strategy, need};
}

class Situations extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get timestamp => dateTime()();
  TextColumn get title => text().nullable()();
}

class Perspectives extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get situation => integer().references(Situations, #id)();
  IntColumn get person => integer().references(People, #id)();
  TextColumn get observation => text().nullable()();
  TextColumn get additionalNotes => text().nullable()();
  BoolColumn get isAppreciation => boolean()();
  BoolColumn get ownDescription => boolean()();
}

class PerspectiveEmotions extends Table {
  IntColumn get perspective => integer().references(Perspectives, #id)();
  TextColumn get emotion => textEnum<Emotion>()();

  @override
  Set<Column>? get primaryKey => {perspective, emotion};
}

class PerspectiveNeeds extends Table {
  IntColumn get perspective => integer().references(Perspectives, #id)();
  TextColumn get need => textEnum<Need>()();

  @override
  Set<Column>? get primaryKey => {perspective, need};
}

@DataClassName("PerspectiveStrategy")
class PerspectiveStrategies extends Table {
  IntColumn get perspective => integer().references(Perspectives, #id)();
  IntColumn get strategy => integer().references(Strategies, #id)();

  @override
  Set<Column>? get primaryKey => {perspective, strategy};
}

@DriftDatabase(tables: [
  Situations,
  People,
  Strategies,
  StrategyNeeds,
  Perspectives,
  PerspectiveEmotions,
  PerspectiveNeeds,
  PerspectiveStrategies
])
class AppDatabase extends _$AppDatabase {
  static const String _databaseFilename = "empado.db";
  static const int _databaseVersion = 1;

  static const _youPerson = Person(id: 0, name: null, pronoun: Pronoun.they);
  static const temporaryYouPerson =
      TemporaryPerson(id: 0, name: null, pronoun: Pronoun.they);

  AppDatabase() : super(_openConnection());

  @override
  int get schemaVersion => _databaseVersion;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        onCreate: (m) async {
          await m.createAll();
        },
        beforeOpen: (details) async {
          await customStatement('PRAGMA foreign_keys = ON');
          if (details.wasCreated) {
            await _createFirstPerson();
          }
        },
      );

  //
  // SITUATIONS
  //

  Future<List<ListSituation>> queryAllFullSituations() async {
    final allSituations = await (select(situations)
          ..orderBy([
            (s) =>
                OrderingTerm(expression: s.timestamp, mode: OrderingMode.desc)
          ]))
        .get();

    final futureList = allSituations.map((situation) async {
      final fullPerspectives = await _queryListPerspectives(situation.id);
      return ListSituation(
          situation: situation, perspectives: fullPerspectives);
    });

    return Future.wait(futureList);
  }

  Future<List<ListPerspective>> queryStrategyPerspectives(
      int strategyId) async {
    final pStrategies = await (select(perspectiveStrategies)
          ..where((tbl) => tbl.strategy.equals(strategyId)))
        .get();

    final futureList = pStrategies.map((pStrategy) async {
      final perspective = await (select(perspectives)
            ..where((tbl) => tbl.id.equals(pStrategy.perspective)))
          .getSingle();
      final emotions = await _queryEmotions(pStrategy.perspective);
      final needs = await _queryNeeds(pStrategy.perspective);
      final person = await _queryPerson(perspective.person);
      return ListPerspective(
          person: person,
          perspective: perspective,
          emotions: emotions,
          needs: needs);
    });

    return Future.wait(futureList);
  }

  Future<void> createOrUpdateSituation(EditableSituation situation) async {
    // Situation
    final origSituationId = situation.id;
    if (origSituationId != null) {
      await deleteSituation(origSituationId);
    }

    final newSituationRowId = await into(situations).insert(SituationsCompanion(
        id: (origSituationId != null)
            ? Value(origSituationId)
            : const Value.absent(),
        timestamp: Value(DateTime.now()),
        title: Value(situation.title)));

    final situationId = origSituationId ??
        (await (select(situations)
                  ..where((s) => s.rowId.equals(newSituationRowId)))
                .getSingle())
            .id;

    for (final fp in situation.perspectives.values) {
      await _createPerspective(situationId, fp);
    }
  }

  Future<void> deleteSituation(int situationId) async {
    final situationPerspectives = await (select(perspectives)
          ..where((p) => p.situation.equals(situationId)))
        .get();

    for (final perspective in situationPerspectives) {
      await batch((batch) {
        batch
          ..deleteWhere(perspectiveEmotions,
              (pe) => pe.perspective.equals(perspective.id))
          ..deleteWhere(
              perspectiveNeeds, (pn) => pn.perspective.equals(perspective.id))
          ..deleteWhere(perspectiveStrategies,
              (ps) => ps.perspective.equals(perspective.id));
      });
      // TODO delete strategies too!
      await (delete(perspectives)..where((p) => p.id.equals(perspective.id)))
          .go();
    }
    await (delete(situations)..where((s) => s.id.equals(situationId))).go();
  }

  //
  // PERSPECTIVES
  //

  Future<List<ListPerspective>> _queryListPerspectives(int situationId) async {
    final situationPerspectives = await (select(perspectives)
          ..where((p) => p.situation.equals(situationId)))
        .get();

    final futureList = situationPerspectives.map((perspective) async {
      final emotions = await _queryEmotions(perspective.id);
      final needs = await _queryNeeds(perspective.id);
      final person = await _queryPerson(perspective.person);
      return ListPerspective(
          person: person,
          perspective: perspective,
          emotions: emotions,
          needs: needs);
    });

    return Future.wait(futureList);
  }

  Future<void> _createPerspective(
      int situationId, final EditablePerspective editablePerspective) async {
    // Person

    final int personId;
    if (editablePerspective.person.id == null) {
      final newRowId = await _createOrUpdatePerson(editablePerspective.person);

      personId = (await (select(people)..where((p) => p.rowId.equals(newRowId)))
              .getSingle())
          .id;
    } else {
      personId = editablePerspective.person.id!;
    }

    // Perspective
    final newPerspectiveRowId =
        await into(perspectives).insertOnConflictUpdate(PerspectivesCompanion(
      id: const Value.absent(),
      situation: Value(situationId),
      person: Value(personId),
      observation: Value(editablePerspective.observation),
      additionalNotes: Value(editablePerspective.additionalNotes),
      isAppreciation: Value(editablePerspective.isAppreciation),
      ownDescription: Value(editablePerspective.ownDescription),
    ));

    final perspectiveId = (await (select(perspectives)
              ..where((p) => p.rowId.equals(newPerspectiveRowId)))
            .getSingle())
        .id;

    // Emotions
    await batch((batch) {
      batch.insertAll(
          perspectiveEmotions,
          editablePerspective.emotions.map((e) => PerspectiveEmotionsCompanion(
              perspective: Value(perspectiveId), emotion: Value(e))));
    });

    // Needs
    await batch((batch) {
      batch.insertAll(
          perspectiveNeeds,
          editablePerspective.needs.map((n) // async
              =>
              PerspectiveNeedsCompanion(
                  perspective: Value(perspectiveId), need: Value(n))));
    });

    // Strategies
    if (editablePerspective.strategies.isNotEmpty) {
      for (final strategy in editablePerspective.strategies) {
        final rowId = await into(strategies).insertOnConflictUpdate(
            strategy.copyWith(lastUsed: Value(DateTime.now())));
        final strategyId = strategy.id.present
            ? strategy.id.value
            : (await (select(strategies)..where((s) => s.rowId.equals(rowId)))
                    .getSingle())
                .id;
        for (final need in editablePerspective.needs) {
          await into(strategyNeeds).insertOnConflictUpdate(
              StrategyNeedsCompanion(
                  strategy: Value(strategyId),
                  need: Value(need),
                  cachedNeedCategory: Value(need.category)));
        }
        await into(perspectiveStrategies).insert(PerspectiveStrategiesCompanion(
            perspective: Value(perspectiveId), strategy: Value(strategyId)));
      }
    }
  }

  //
  // PEOPLE
  //

  Future<void> _createFirstPerson() async {
    await (into(people).insert(_youPerson));
  }

  Future<Person> _queryPerson(int id) async {
    return await (select(people)..where((p) => p.id.equals(id))).getSingle();
  }

  Future<List<Person>> queryAllPeople() async {
    return await select(people).get();
  }

  Future<int> _createOrUpdatePerson(TemporaryPerson person) async {
    final id = person.id;

    assert(person.name != null || id == _youPerson.id);

    return await into(people).insertOnConflictUpdate(PeopleCompanion(
        id: id == null ? const Value.absent() : Value(id),
        name: Value(person.name),
        pronoun: Value(person.pronoun)));
  }

  //
  // EMOTIONS
  //

  Future<List<PerspectiveEmotion>> _queryEmotions(int perspective) async {
    return await (select(perspectiveEmotions)
          ..where((pe) => pe.perspective.equals(perspective)))
        .get();
  }

  //
  // NEEDS
  //

  Future<List<PerspectiveNeed>> _queryNeeds(int perspective) async {
    return await (select(perspectiveNeeds)
          ..where((pn) => pn.perspective.equals(perspective)))
        .get();
  }

  //
  // STRATEGIES
  //
  Future<List<ListStrategy>> queryAllStrategiesByLastUsed() async {
    final strategyList = await (select(strategies)
          ..orderBy([(s) => OrderingTerm.desc(s.lastUsed)]))
        .get();
    final futureList = strategyList.map((s) async => ListStrategy(
        strategy: s,
        strategyNeeds: await (select(strategyNeeds)
              ..where((sn) => sn.strategy.equals(s.id)))
            .get()));

    return Future.wait(futureList);
  }

  Future<List<Strategy>> queryPerspectiveStrategies(int perspectiveId) async {
    final ps = await (select(perspectiveStrategies)
          ..where((item) => item.perspective.equals(perspectiveId)))
        .get();
    final futureList = ps.map((item) => (select(strategies)
          ..where((s) => s.id.equals(item.strategy)))
        .getSingle());
    return Future.wait(futureList);
  }

  Future<void> toggleStrategyArchive(Strategy strategy, bool archived) async {
    await (update(strategies)..whereSamePrimaryKey(strategy))
        .write(StrategiesCompanion(archived: Value(archived)));
  }

  Future<void> toggleStrategyBookmark(
      Strategy strategy, bool bookmarked) async {
    await (update(strategies)..whereSamePrimaryKey(strategy))
        .write(StrategiesCompanion(bookmarked: Value(bookmarked)));
  }
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final sqlFile =
        File(p.join(documentsDirectory.path, AppDatabase._databaseFilename));
    return NativeDatabase(sqlFile);
  });
}
