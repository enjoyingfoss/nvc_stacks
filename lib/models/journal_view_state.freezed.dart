// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'journal_view_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$JournalViewState {
  TemporaryPerson get currentPerson => throw _privateConstructorUsedError;
  bool get dontDisplaySheet => throw _privateConstructorUsedError;
  JournalTab get currentTab => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $JournalViewStateCopyWith<JournalViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JournalViewStateCopyWith<$Res> {
  factory $JournalViewStateCopyWith(
          JournalViewState value, $Res Function(JournalViewState) then) =
      _$JournalViewStateCopyWithImpl<$Res, JournalViewState>;
  @useResult
  $Res call(
      {TemporaryPerson currentPerson,
      bool dontDisplaySheet,
      JournalTab currentTab});

  $TemporaryPersonCopyWith<$Res> get currentPerson;
}

/// @nodoc
class _$JournalViewStateCopyWithImpl<$Res, $Val extends JournalViewState>
    implements $JournalViewStateCopyWith<$Res> {
  _$JournalViewStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currentPerson = null,
    Object? dontDisplaySheet = null,
    Object? currentTab = null,
  }) {
    return _then(_value.copyWith(
      currentPerson: null == currentPerson
          ? _value.currentPerson
          : currentPerson // ignore: cast_nullable_to_non_nullable
              as TemporaryPerson,
      dontDisplaySheet: null == dontDisplaySheet
          ? _value.dontDisplaySheet
          : dontDisplaySheet // ignore: cast_nullable_to_non_nullable
              as bool,
      currentTab: null == currentTab
          ? _value.currentTab
          : currentTab // ignore: cast_nullable_to_non_nullable
              as JournalTab,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $TemporaryPersonCopyWith<$Res> get currentPerson {
    return $TemporaryPersonCopyWith<$Res>(_value.currentPerson, (value) {
      return _then(_value.copyWith(currentPerson: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$JournalViewStateImplCopyWith<$Res>
    implements $JournalViewStateCopyWith<$Res> {
  factory _$$JournalViewStateImplCopyWith(_$JournalViewStateImpl value,
          $Res Function(_$JournalViewStateImpl) then) =
      __$$JournalViewStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {TemporaryPerson currentPerson,
      bool dontDisplaySheet,
      JournalTab currentTab});

  @override
  $TemporaryPersonCopyWith<$Res> get currentPerson;
}

/// @nodoc
class __$$JournalViewStateImplCopyWithImpl<$Res>
    extends _$JournalViewStateCopyWithImpl<$Res, _$JournalViewStateImpl>
    implements _$$JournalViewStateImplCopyWith<$Res> {
  __$$JournalViewStateImplCopyWithImpl(_$JournalViewStateImpl _value,
      $Res Function(_$JournalViewStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currentPerson = null,
    Object? dontDisplaySheet = null,
    Object? currentTab = null,
  }) {
    return _then(_$JournalViewStateImpl(
      currentPerson: null == currentPerson
          ? _value.currentPerson
          : currentPerson // ignore: cast_nullable_to_non_nullable
              as TemporaryPerson,
      dontDisplaySheet: null == dontDisplaySheet
          ? _value.dontDisplaySheet
          : dontDisplaySheet // ignore: cast_nullable_to_non_nullable
              as bool,
      currentTab: null == currentTab
          ? _value.currentTab
          : currentTab // ignore: cast_nullable_to_non_nullable
              as JournalTab,
    ));
  }
}

/// @nodoc

class _$JournalViewStateImpl implements _JournalViewState {
  const _$JournalViewStateImpl(
      {this.currentPerson = AppDatabase.temporaryYouPerson,
      this.dontDisplaySheet = false,
      this.currentTab = JournalTab.emotions});

  @override
  @JsonKey()
  final TemporaryPerson currentPerson;
  @override
  @JsonKey()
  final bool dontDisplaySheet;
  @override
  @JsonKey()
  final JournalTab currentTab;

  @override
  String toString() {
    return 'JournalViewState(currentPerson: $currentPerson, dontDisplaySheet: $dontDisplaySheet, currentTab: $currentTab)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$JournalViewStateImpl &&
            (identical(other.currentPerson, currentPerson) ||
                other.currentPerson == currentPerson) &&
            (identical(other.dontDisplaySheet, dontDisplaySheet) ||
                other.dontDisplaySheet == dontDisplaySheet) &&
            (identical(other.currentTab, currentTab) ||
                other.currentTab == currentTab));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, currentPerson, dontDisplaySheet, currentTab);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$JournalViewStateImplCopyWith<_$JournalViewStateImpl> get copyWith =>
      __$$JournalViewStateImplCopyWithImpl<_$JournalViewStateImpl>(
          this, _$identity);
}

abstract class _JournalViewState implements JournalViewState {
  const factory _JournalViewState(
      {final TemporaryPerson currentPerson,
      final bool dontDisplaySheet,
      final JournalTab currentTab}) = _$JournalViewStateImpl;

  @override
  TemporaryPerson get currentPerson;
  @override
  bool get dontDisplaySheet;
  @override
  JournalTab get currentTab;
  @override
  @JsonKey(ignore: true)
  _$$JournalViewStateImplCopyWith<_$JournalViewStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
