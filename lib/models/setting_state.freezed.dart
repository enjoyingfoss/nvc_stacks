// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'setting_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SettingState {
  Locale? get preferredLocale => throw _privateConstructorUsedError;
  ThemeVariant get themeVariant => throw _privateConstructorUsedError;
  bool get useSystemColors => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingStateCopyWith<SettingState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingStateCopyWith<$Res> {
  factory $SettingStateCopyWith(
          SettingState value, $Res Function(SettingState) then) =
      _$SettingStateCopyWithImpl<$Res, SettingState>;
  @useResult
  $Res call(
      {Locale? preferredLocale,
      ThemeVariant themeVariant,
      bool useSystemColors});
}

/// @nodoc
class _$SettingStateCopyWithImpl<$Res, $Val extends SettingState>
    implements $SettingStateCopyWith<$Res> {
  _$SettingStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? preferredLocale = freezed,
    Object? themeVariant = null,
    Object? useSystemColors = null,
  }) {
    return _then(_value.copyWith(
      preferredLocale: freezed == preferredLocale
          ? _value.preferredLocale
          : preferredLocale // ignore: cast_nullable_to_non_nullable
              as Locale?,
      themeVariant: null == themeVariant
          ? _value.themeVariant
          : themeVariant // ignore: cast_nullable_to_non_nullable
              as ThemeVariant,
      useSystemColors: null == useSystemColors
          ? _value.useSystemColors
          : useSystemColors // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingStateImplCopyWith<$Res>
    implements $SettingStateCopyWith<$Res> {
  factory _$$SettingStateImplCopyWith(
          _$SettingStateImpl value, $Res Function(_$SettingStateImpl) then) =
      __$$SettingStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Locale? preferredLocale,
      ThemeVariant themeVariant,
      bool useSystemColors});
}

/// @nodoc
class __$$SettingStateImplCopyWithImpl<$Res>
    extends _$SettingStateCopyWithImpl<$Res, _$SettingStateImpl>
    implements _$$SettingStateImplCopyWith<$Res> {
  __$$SettingStateImplCopyWithImpl(
      _$SettingStateImpl _value, $Res Function(_$SettingStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? preferredLocale = freezed,
    Object? themeVariant = null,
    Object? useSystemColors = null,
  }) {
    return _then(_$SettingStateImpl(
      preferredLocale: freezed == preferredLocale
          ? _value.preferredLocale
          : preferredLocale // ignore: cast_nullable_to_non_nullable
              as Locale?,
      themeVariant: null == themeVariant
          ? _value.themeVariant
          : themeVariant // ignore: cast_nullable_to_non_nullable
              as ThemeVariant,
      useSystemColors: null == useSystemColors
          ? _value.useSystemColors
          : useSystemColors // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SettingStateImpl implements _SettingState {
  const _$SettingStateImpl(
      {this.preferredLocale = null,
      this.themeVariant = ThemeVariant.system,
      this.useSystemColors = false});

  @override
  @JsonKey()
  final Locale? preferredLocale;
  @override
  @JsonKey()
  final ThemeVariant themeVariant;
  @override
  @JsonKey()
  final bool useSystemColors;

  @override
  String toString() {
    return 'SettingState(preferredLocale: $preferredLocale, themeVariant: $themeVariant, useSystemColors: $useSystemColors)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingStateImpl &&
            (identical(other.preferredLocale, preferredLocale) ||
                other.preferredLocale == preferredLocale) &&
            (identical(other.themeVariant, themeVariant) ||
                other.themeVariant == themeVariant) &&
            (identical(other.useSystemColors, useSystemColors) ||
                other.useSystemColors == useSystemColors));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, preferredLocale, themeVariant, useSystemColors);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingStateImplCopyWith<_$SettingStateImpl> get copyWith =>
      __$$SettingStateImplCopyWithImpl<_$SettingStateImpl>(this, _$identity);
}

abstract class _SettingState implements SettingState {
  const factory _SettingState(
      {final Locale? preferredLocale,
      final ThemeVariant themeVariant,
      final bool useSystemColors}) = _$SettingStateImpl;

  @override
  Locale? get preferredLocale;
  @override
  ThemeVariant get themeVariant;
  @override
  bool get useSystemColors;
  @override
  @JsonKey(ignore: true)
  _$$SettingStateImplCopyWith<_$SettingStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
