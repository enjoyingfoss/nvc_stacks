// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/need_category.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum Need {
  health(NeedCategory.physiology),
  air(NeedCategory.physiology),
  exercise(NeedCategory.physiology),
  food(NeedCategory.physiology),
  physicalSafety(NeedCategory.physiology),
  rest(NeedCategory.physiology),
  shelter(NeedCategory.physiology),
  sex(NeedCategory.physiology),
  touch(NeedCategory.physiology),
  warmth(NeedCategory.physiology),
  water(NeedCategory.physiology),

  security(NeedCategory.security),
  certainty(NeedCategory.security),
  hope(NeedCategory.security),
  innerPeace(NeedCategory.security),
  safety(NeedCategory.security),
  stability(NeedCategory.security),
  support(NeedCategory.security),

  belonging(NeedCategory.loveBelonging),
  acceptance(NeedCategory.loveBelonging),
  affection(NeedCategory.loveBelonging),
  care(NeedCategory.loveBelonging),
  closeness(NeedCategory.loveBelonging),
  collaboration(NeedCategory.loveBelonging),
  communication(NeedCategory.loveBelonging),
  community(NeedCategory.loveBelonging),
  company(NeedCategory.loveBelonging),
  compassion(NeedCategory.loveBelonging),
  consideration(NeedCategory.loveBelonging),
  empathy(NeedCategory.loveBelonging),
  harmony(NeedCategory.loveBelonging),
  inclusion(NeedCategory.loveBelonging),
  intimacy(NeedCategory.loveBelonging),
  love(NeedCategory.loveBelonging),
  participation(NeedCategory.loveBelonging),
  partnership(NeedCategory.loveBelonging),
  presence(NeedCategory.loveBelonging),
  reciprocity(NeedCategory.loveBelonging),

  respect(NeedCategory.esteem),
  attention(NeedCategory.esteem),
  competence(NeedCategory.esteem),
  recognition(NeedCategory.esteem),
  trust(NeedCategory.esteem),
  known(NeedCategory.esteem),
  understood(NeedCategory.esteem),

  stimulation(NeedCategory.cognitive),
  awareness(NeedCategory.cognitive),
  challenge(NeedCategory.cognitive),
  clarity(NeedCategory.cognitive),
  creativity(NeedCategory.cognitive),
  discovery(NeedCategory.cognitive),
  inspiration(NeedCategory.cognitive),
  learning(NeedCategory.cognitive),
  playfulness(NeedCategory.cognitive),
  selfExpression(NeedCategory.cognitive),
    know(NeedCategory.cognitive),
  understand(NeedCategory.cognitive),

  beauty(NeedCategory.beautyEnvironment),
  consistency(NeedCategory.beautyEnvironment),
  order(NeedCategory.beautyEnvironment),
  quiet(NeedCategory.beautyEnvironment),
  space(NeedCategory.beautyEnvironment),

  growth(NeedCategory.actualization),
  celebration(NeedCategory.actualization),
  choice(NeedCategory.actualization),
  ease(NeedCategory.actualization),
  effectiveness(NeedCategory.actualization),
  efficiency(NeedCategory.actualization),
  freedom(NeedCategory.actualization),
  humor(NeedCategory.actualization),
  independence(NeedCategory.actualization),
  mourning(NeedCategory.actualization),
  privacy(NeedCategory.actualization),
  spontaneity(NeedCategory.actualization),

  purpose(NeedCategory.transcendence),
  authenticity(NeedCategory.transcendence),
  contribution(NeedCategory.transcendence),
  equality(NeedCategory.transcendence),
  fairness(NeedCategory.transcendence),
  honesty(NeedCategory.transcendence),
  integrity(NeedCategory.transcendence),
  mattering(NeedCategory.transcendence);

  final NeedCategory category;

  const Need(this.category);

  String getTranslation(BuildContext context) {
    final localizations = AppLocalizations.of(context);
    switch (this) {
      case Need.air:
        return localizations.needAir;
      case Need.food:
        return localizations.needFood;
      case Need.exercise:
        return localizations.needExercise;
      case Need.rest:
        return localizations.needRest;
      case Need.health:
        return localizations.needHealth;
      case Need.sex:
        return localizations.needSex;
      case Need.physicalSafety:
        return localizations.needPhysicalSafety;
      case Need.shelter:
        return localizations.needHome;
      case Need.touch:
        return localizations.needTouch;
      case Need.water:
        return localizations.needWater;

      case Need.innerPeace:
        return localizations.needInnerPeace;
      case Need.safety:
        return localizations.needSafety;
      case Need.security:
        return localizations.needSecurity;
      case Need.certainty:
        return localizations.needCertainty;
      case Need.stability:
        return localizations.needStability;
      case Need.support:
        return localizations.needSupport;

      case Need.choice:
        return localizations.needChoice;
      case Need.freedom:
        return localizations.needFreedom;
      case Need.space:
        return localizations.needSpace;
      case Need.spontaneity:
        return localizations.needSpontaneity;
      case Need.independence:
        return localizations.needIndependence;

      case Need.acceptance:
        return localizations.needAcceptance;
      case Need.affection:
        return localizations.needAffection;
      case Need.belonging:
        return localizations.needBelonging;
      case Need.collaboration:
        return localizations.needCollaboration;
      case Need.communication:
        return localizations.needCommunication;
      case Need.community:
        return localizations.needCommunity;
      case Need.closeness:
        return localizations.needCloseness;
      case Need.company:
        return localizations.needCompany;
      case Need.partnership:
        return localizations.needPartnership;
      case Need.compassion:
        return localizations.needCompassion;
      case Need.consideration:
        return localizations.needConsideration;
      case Need.empathy:
        return localizations.needEmpathy;
      case Need.inclusion:
        return localizations.needInclusion;
      case Need.intimacy:
        return localizations.needIntimacy;
      case Need.love:
        return localizations.needLove;
      case Need.reciprocity:
        return localizations.needReciprocity;
      case Need.care:
        return localizations.needCare;
      case Need.warmth:
        return localizations.needWarmth;

      case Need.recognition:
        return localizations.needRecognition;
      case Need.competence:
        return localizations.needCompetence;
      case Need.respect:
        return localizations.needRespect;
      case Need.known:
        return localizations.needKnown;
      case Need.attention:
        return localizations.needAttention;
      case Need.understood:
        return localizations.needUnderstood;
      case Need.trust:
        return localizations.needTrust;

      case Need.fairness:
        return localizations.needFairness;
      case Need.beauty:
        return localizations.needBeauty;
      case Need.consistency:
        return localizations.needConsistency;
      case Need.equality:
        return localizations.needEquality;
      case Need.harmony:
        return localizations.needHarmony;
      case Need.order:
        return localizations.needOrder;

      case Need.quiet:
        return localizations.needQuiet;

      case Need.authenticity:
        return localizations.needAuthenticity;
      case Need.honesty:
        return localizations.needHonesty;
      case Need.awareness:
        return localizations.needAwareness;
      case Need.clarity:
        return localizations.needClarity;
      case Need.discovery:
        return localizations.needDiscovery;
      case Need.integrity:
        return localizations.needIntegrity;
      case Need.learning:
        return localizations.needLearning;
      case Need.presence:
        return localizations.needPresence;
      case Need.understand:
        return localizations.needUnderstand;
      case Need.know:
        return localizations.needKnow;

      case Need.celebration:
        return localizations.needCelebration;
      case Need.challenge:
        return localizations.needChallenge;
      case Need.creativity:
        return localizations.needCreativity;
      case Need.ease:
        return localizations.needEase;
      case Need.growth:
        return localizations.needGrowth;
      case Need.hope:
        return localizations.needHope;
      case Need.inspiration:
        return localizations.needInspiration;
      case Need.privacy:
        return localizations.needPrivacy;
      case Need.humor:
        return localizations.needHumor;
      case Need.mourning:
        return localizations.needMourning;
      case Need.selfExpression:
        return localizations.needSelfExpression;
      case Need.playfulness:
        return localizations.needPlayfulness;
      case Need.stimulation:
        return localizations.needStimulation;

      case Need.contribution:
        return localizations.needContribution;
      case Need.efficiency:
        return localizations.needEfficiency;
      case Need.effectiveness:
        return localizations.needEffectiveness;
      case Need.participation:
        return localizations.needParticipation;
      case Need.purpose:
        return localizations.needPurpose;
      case Need.mattering:
        return localizations.needMattering;
    }
  }
}
