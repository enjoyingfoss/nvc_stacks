// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/need.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum NeedCategory {
  physiology("assets/category_physiology.svg"),
  security("assets/category_security.svg"),
  loveBelonging("assets/category_belonging.svg"),
  esteem("assets/category_esteem.svg"),
  cognitive("assets/category_cognitive.svg"),
  beautyEnvironment("assets/category_beauty.svg"),
  actualization("assets/category_actualization.svg"),
  transcendence("assets/category_transcendence.svg");

  static final Map<NeedCategory, List<Need>> groups = _getGroups();

  final String assetPath;

  const NeedCategory(this.assetPath);

  static Map<NeedCategory, List<Need>> _getGroups() {
    final groupMap = {
      for (final nc in NeedCategory.values) nc: List<Need>.empty(growable: true)
    };
    for (final need in Need.values) {
      groupMap[need.category]!.add(need);
    }
    return groupMap;
  }

  String getTranslation(BuildContext context) {
    switch (this) {
      case NeedCategory.physiology:
        return AppLocalizations.of(context).needCategoryPhysiology;
      case NeedCategory.security:
        return AppLocalizations.of(context).needCategorySecurity;
      case NeedCategory.loveBelonging:
        return AppLocalizations.of(context).needCategoryLoveBelonging;
      case NeedCategory.esteem:
        return AppLocalizations.of(context).needCategoryEsteem;
      case NeedCategory.cognitive:
        return AppLocalizations.of(context).needCategoryCognitive;
      case NeedCategory.beautyEnvironment:
        return AppLocalizations.of(context).needCategoryBeautyEnvironment;
      case NeedCategory.actualization:
        return AppLocalizations.of(context).needCategoryActualization;
      case NeedCategory.transcendence:
        return AppLocalizations.of(context).needCategoryTranscendence;
    }
  }
}
