// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/emotion.dart';
import 'package:empado/screens/journal/components/emotion_picker/emotion_sliders.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EmotionPicker extends StatefulWidget {
  final double cancelAreaHeight;
  final Function(Emotion) onValueSet;
  final Function()? onDrag;
  final Function()? onCancelled;

  const EmotionPicker(
      {required this.cancelAreaHeight,
      required this.onValueSet,
      this.onDrag,
      this.onCancelled,
      super.key});

  @override
  State<EmotionPicker> createState() => _EmotionPickerState();
}

class _EmotionPickerState extends State<EmotionPicker> {
  Emotion? _emotion;
  bool _dragging = false;

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;

    return Padding(
        padding: EdgeInsets.fromLTRB(
            16, 8, 16, MediaQuery.of(context).viewPadding.bottom + 8),
        child: Column(children: [
          Text(
            _emotion?.getTranslation(context) ??
                (_dragging ? AppLocalizations.of(context).cancel : ""),
            style: Theme.of(context)
                .textTheme
                .titleMedium
                ?.copyWith(fontWeight: FontWeight.w700),
          ),
          Expanded(
              child: Center(
                  child: AspectRatio(
                      aspectRatio: 1,
                      child: EmotionSliders(onDrag: (emotion) {
                        widget.onDrag?.call();
                        setState(() {
                          _emotion = emotion;
                          _dragging = true;
                        });
                      }, onValueSet: (emotion) {
                        setState(() {
                          if (emotion != null) {
                            widget.onValueSet(emotion);
                          } else {
                            widget.onCancelled?.call();
                          }
                          _emotion = null;
                          _dragging = false;
                        });
                      })))),
          const SizedBox(height: 4),
          if (_dragging)
            Container(
                height: widget.cancelAreaHeight,
                alignment: Alignment.center,
                child: Row(mainAxisSize: MainAxisSize.min, children: [
                  Icon(_emotion == null ? Icons.cancel : Icons.cancel_outlined,
                      color: _emotion == null
                          ? colorScheme.primary
                          : colorScheme.onSurfaceVariant),
                  const SizedBox(width: 4),
                  Text(
                    AppLocalizations.of(context).dragHereToCancel,
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        color: _emotion == null
                            ? colorScheme.primary
                            : colorScheme.onSurfaceVariant),
                  )
                ]))
          else
            SizedBox(height: widget.cancelAreaHeight)
        ]));
  }
}
