// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:math';

import 'package:empado/enums/emotion_category.dart';
import 'package:empado/enums/emotion_intensity.dart';
import 'package:empado/screens/journal/components/emotion_picker/emotion_handle.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//TODO integrate haptic feedback? https://pub.dev/packages/haptic_feedback
class EmotionBackgroundPainter extends CustomPainter {
  static const _viewbox = 288.0;
  static const _grayColor = Color(0x7f000000);
  static const _centerRect = Rect.fromLTWH(0, 0, _viewbox, _viewbox);
  static const _rightRect =
      Rect.fromLTWH(-_viewbox, 0, _viewbox * 2, _viewbox * 2);

  final EmotionCategory? curEmotionCategory;
  final int categoryIndexOffset;

  EmotionBackgroundPainter(
      {required this.categoryIndexOffset, this.curEmotionCategory});

  @override
  void paint(Canvas canvas, Size size) {
    if (curEmotionCategory != null) {
      _paintLeaf(canvas, size, curEmotionCategory!);
    } else {
      _paintBackground(canvas, size);
    }
  }

  @override
  bool shouldRepaint(EmotionBackgroundPainter oldDelegate) {
    return oldDelegate.curEmotionCategory != curEmotionCategory;
  }

  LinearGradient _getBackgroundGradient(Color center, Color outside) {
    return LinearGradient(
      // center: Alignment.center,
      // radius: 0.5,
      colors: [
        center,
        outside,
      ],
      stops: const [0.75, 1.0],
    );
  }

  void _paintBackground(Canvas canvas, Size size) {
    canvas.scale((size.width / _viewbox), (size.height / _viewbox));

    for (int i = 0; i < EmotionCategory.values.length; i++) {
      final category =
          EmotionCategory.values[(i + 2) % EmotionCategory.values.length];

      final fillPaint = Paint()
        ..shader = _getBackgroundGradient(category.backgroundColor, _grayColor)
            .createShader(_centerRect);

      final outlinePaint = Paint()
        ..color = const Color(0x1f000000)
        ..style = PaintingStyle.stroke
        ..isAntiAlias = false
        ..strokeWidth = 0.5;

      //TODO figure out spacing here

      final path0 = Path()
        ..moveTo(159.812, 140.165)
        ..lineTo(157.658, 141.395)
        ..cubicTo(155.643, 142.547, 155.643, 145.453, 157.658, 146.605)
        ..lineTo(159.812, 147.835)
        ..cubicTo(161.812, 148.978, 164.3, 147.534, 164.3, 145.23)
        ..lineTo(164.3, 142.77)
        ..cubicTo(164.3, 140.466, 161.812, 139.022, 159.812, 140.165)
        ..close();

      canvas
        ..drawPath(path0, fillPaint)
        ..drawPath(path0, outlinePaint);

      final path1 = Path()
        ..moveTo(176.212, 140.965)
        ..lineTo(175.458, 141.395)
        ..cubicTo(173.443, 142.547, 173.443, 145.453, 175.458, 146.605)
        ..lineTo(176.212, 147.035)
        ..cubicTo(178.212, 148.178, 180.7, 146.734, 180.7, 144.43)
        ..lineTo(180.7, 143.57)
        ..cubicTo(180.7, 141.266, 178.212, 139.822, 176.212, 140.965)
        ..close();

      canvas
        ..drawPath(path1, fillPaint)
        ..drawPath(path1, outlinePaint);

      final path2 = Path()
        ..moveTo(192.932, 146.419)
        ..cubicTo(194.789, 147.48, 197.1, 146.139, 197.1, 144)
        ..cubicTo(197.1, 141.861, 194.789, 140.52, 192.932, 141.581)
        ..cubicTo(191.061, 142.651, 191.061, 145.349, 192.932, 146.419)
        ..close();

      canvas
        ..drawPath(path2, fillPaint)
        ..drawPath(path2, outlinePaint);

      final path3 = Path()
        ..moveTo(210.027, 146.015)
        ..cubicTo(211.575, 146.9, 213.5, 145.782, 213.5, 144)
        ..cubicTo(213.5, 142.218, 211.575, 141.1, 210.027, 141.985)
        ..cubicTo(208.468, 142.876, 208.468, 145.124, 210.027, 146.015)
        ..close();

      canvas
        ..drawPath(path3, fillPaint)
        ..drawPath(path3, outlinePaint);

      final path4 = Path()
        ..moveTo(227.122, 145.612)
        ..cubicTo(228.36, 146.32, 229.9, 145.426, 229.9, 144)
        ..cubicTo(229.9, 142.574, 228.36, 141.68, 227.122, 142.388)
        ..cubicTo(225.874, 143.101, 225.874, 144.899, 227.122, 145.612)
        ..close();

      canvas
        ..drawPath(path4, fillPaint)
        ..drawPath(path4, outlinePaint);

      final path5 = Path()
        ..moveTo(244.216, 145.209)
        ..cubicTo(245.145, 145.74, 246.3, 145.069, 246.3, 144)
        ..cubicTo(246.3, 142.931, 245.145, 142.26, 244.216, 142.791)
        ..cubicTo(243.281, 143.325, 243.281, 144.675, 244.216, 145.209)
        ..close();

      canvas
        ..drawPath(path5, fillPaint)
        ..drawPath(path5, outlinePaint);

      final path6 = Path()
        ..moveTo(261.311, 144.806)
        ..cubicTo(261.93, 145.16, 262.7, 144.713, 262.7, 144)
        ..cubicTo(262.7, 143.287, 261.93, 142.84, 261.311, 143.194)
        ..cubicTo(260.687, 143.55, 260.687, 144.45, 261.311, 144.806)
        ..close();

      canvas
        ..drawPath(path6, fillPaint)
        ..drawPath(path6, outlinePaint);

      final r = sqrt(2) * _viewbox / 2;
      final shiftX = r * cos(pi / 2);
      final shiftY = r * sin(pi / 2);
      final translateX = _viewbox / 2 - shiftX;
      final translateY = _viewbox / 2 - shiftY;

      canvas
        ..translate(translateX, translateY)
        ..rotate(pi / 4);
    }
  }

  void _paintLeaf(Canvas canvas, Size size, EmotionCategory emotionCategory) {
    final angle = pi /
        4 *
        ((emotionCategory.index + categoryIndexOffset) %
            EmotionCategory.values.length);
    final r = sqrt(2) * _viewbox / 2;
    final beta = pi / 4 + angle;

    final shiftX = r * cos(beta);
    final shiftY = r * sin(beta);
    final translateX = _viewbox / 2 - shiftX;
    final translateY = _viewbox / 2 - shiftY;

    canvas
      ..scale((size.width / _viewbox), (size.height / _viewbox))
      ..translate(translateX, translateY)
      ..rotate(angle);

    final fillPaint = Paint()
      ..shader =
          _getBackgroundGradient(emotionCategory.backgroundColor, _grayColor)
              .createShader(_rightRect);

    final outlinePaint = Paint()
      ..color = const Color(0x3f000000)
      ..style = PaintingStyle.stroke
      ..isAntiAlias = false
      ..strokeWidth = 0.5;

    const y = _viewbox / 2;
    const rInit = 2;
    const rAdd = 1.5;

    final intensityCount = EmotionIntensity.values.length;
    final convertedHandleSize =
        EmotionHandle.size * _viewbox / min(size.width, size.height);
    final xInit = _viewbox - convertedHandleSize / 2;
    final xAdd = -(_viewbox - convertedHandleSize) / (intensityCount - 1);

    for (int i = 0; i < intensityCount; i++) {
      canvas
        ..drawCircle(Offset(xInit + xAdd * i, y), (rInit + rAdd * i).toDouble(),
            fillPaint)
        ..drawCircle(Offset(xInit + xAdd * i, y), (rInit + rAdd * i).toDouble(),
            outlinePaint);
    }
  }
}
