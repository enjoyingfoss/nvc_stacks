// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/enums/journal_tab.dart';
import 'package:empado/enums/nav_item.dart';
import 'package:empado/providers/journal_view_provider.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/context_cards.dart';
import 'package:empado/screens/journal/components/emotion_picker/emotion_picker.dart';
import 'package:empado/screens/journal/components/need_picker.dart';
import 'package:empado/screens/journal/components/person_picker.dart';
import 'package:empado/screens/journal/components/situation_sheet/situation_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class JournalSubscreen extends ConsumerStatefulWidget {
  const JournalSubscreen({super.key});

  @override
  ConsumerState<JournalSubscreen> createState() => _JournalScreenState();
}

class _JournalScreenState extends ConsumerState<JournalSubscreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        vsync: this,
        length: JournalTab.values.length,
        initialIndex: ref.read(journalViewStateProvider).currentTab.index)
      ..addListener(() {
        ref
            .read(journalViewStateProvider.notifier)
            .setTab(JournalTab.values[_tabController.index]);
      });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final perspectiveCount =
        ref.watch(situationProvider).state.perspectives.keys.length;
    final person = ref
        .watch(journalViewStateProvider.select((value) => value.currentPerson));
    final situationNotifier = ref.read(situationProvider.notifier);
    final homeViewNotifier = ref.read(journalViewStateProvider.notifier);

    //TODO preserve state of current journal in the DB marked as a "draft"

    return SituationSheet(
        person: person,
        body: Column(
          children: [
            AppBar(
              title: Text(person.isYou()
                  ? AppLocalizations.of(context).whatsGoingOnForYou
                  : AppLocalizations.of(context).whatsGoingOnForThem(
                      person.name!, person.pronoun.localeKey)),
              actions: [
                IconButton(
                    tooltip: AppLocalizations.of(context).peopleInvolved,
                    onPressed: () async {
                      await showModalBottomSheet(
                          context: context,
                          builder: (context) => const PersonPicker());
                    },
                    icon: Padding(
                        padding: const EdgeInsets.all(8),
                        child: perspectiveCount > 1
                            ? Row(children: [
                                const Icon(Icons.people),
                                const SizedBox(width: 4),
                                Text(perspectiveCount.toString())
                              ])
                            : const Icon(Icons.people)))
              ],
            ),
            TabBar(
              controller: _tabController,
              tabs: JournalTab.values
                  .map((tab) => Tab(text: tab.getTranslation(context)))
                  .toList(),
            ),
            Expanded(
                child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                EmotionPicker(
                  cancelAreaHeight: SituationSheet.height,
                  onDrag: () {
                    homeViewNotifier.setDontDisplaySheet(true);
                  },
                  onValueSet: (e) {
                    homeViewNotifier.setDontDisplaySheet(false);
                    situationNotifier.addEmotion(person, e);
                  },
                  onCancelled: () {
                    homeViewNotifier.setDontDisplaySheet(false);
                  },
                ),
                NeedPicker(person: person),
                ContextCards(person: person)
              ],
            ))
          ],
        ));
  }
  //TODO redesign based on designs
}
