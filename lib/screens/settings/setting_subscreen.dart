// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'dart:io';

import 'package:empado/components/error_content.dart';
import 'package:empado/enums/theme_variant.dart';
import 'package:empado/providers/setting_provider.dart';
import 'package:empado/screens/settings/components/language_dialog.dart';
import 'package:empado/screens/settings/components/theme_dialog.dart';
import 'package:empado/utils/language_util.dart';
import 'package:empado/utils/url_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:package_info_plus/package_info_plus.dart';

class SettingSubscreen extends ConsumerWidget {
  const SettingSubscreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncSettingState = ref.watch(settingProvider);
    final asyncSettingNotifier = ref.read(settingProvider.notifier);

    return switch (asyncSettingState) {
      AsyncData(value: final settingState) => CustomScrollView(
          slivers: [
            SliverAppBar(
              title: Text(AppLocalizations.of(context).settings),
            ),
            SliverList.list(
              children: [
                ListTile(
                  leading: const Icon(Icons.language),
                  title: Text(AppLocalizations.of(context).language),
                  subtitle: Text((settingState.preferredLocale == null)
                      ? AppLocalizations.of(context).languageSystem
                      : AppLocalizations.of(context).languageItem(
                          LanguageUtil.getLanguageName(
                              context, settingState.preferredLocale!),
                          settingState.preferredLocale!.languageCode
                              .toUpperCase())),
                  onTap: () async {
                    await showDialog<Locale?>(
                        context: context,
                        builder: (_) => LanguageDialog(
                            curSettingState: settingState,
                            curLocale: settingState.preferredLocale));
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.style),
                  title: Text(AppLocalizations.of(context).theme),
                  subtitle:
                      Text(settingState.themeVariant.getTranslation(context)),
                  onTap: () async {
                    final newVariant = await showDialog<ThemeVariant>(
                        context: context,
                        builder: (_) =>
                            ThemeDialog(curVariant: settingState.themeVariant));
                    if (newVariant != null) {
                      unawaited(asyncSettingNotifier.setThemeVariant(
                          settingState, newVariant));
                    }
                  },
                ),
                if (!Platform.isIOS)
                  SwitchListTile.adaptive(
                      secondary: const SizedBox(),
                      title: Text(AppLocalizations.of(context).systemColors),
                      value: settingState.useSystemColors,
                      onChanged: (useSystemColors) {
                        unawaited(asyncSettingNotifier.setUseSystemColors(
                            settingState, useSystemColors));
                      }),
                ListTile(
                    leading: const Icon(Icons.savings),
                    title: Text(AppLocalizations.of(context).donate),
                    onTap: () async => await URLUtil.launchURL(
                        context, "https://liberapay.com/Empado/")),
                ListTile(
                    leading: const Icon(Icons.code),
                    title: Text(AppLocalizations.of(context).sourceCode),
                    onTap: () async => await URLUtil.launchURL(
                        context, "https://gitlab.com/enjoyingfoss/empado")),
                ListTile(
                  leading: const Icon(Icons.info),
                  title: Text(AppLocalizations.of(context).aboutEmpado),
                  onTap: () async {
                    final packageInfo = await PackageInfo.fromPlatform();
                    if (context.mounted) {
                      showAboutDialog(
                          context: context,
                          applicationName: packageInfo.appName,
                          applicationVersion: packageInfo.version,
                          applicationIcon:
                              SvgPicture.asset('assets/icon.svg', width: 48),
                          applicationLegalese:
                              AppLocalizations.of(context).licensedAGPLv3);
                    }
                  },
                ),
              ],
            )
          ],
        ),
      AsyncError() => const Center(child: ErrorContent()),
      _ => const Center(child: CircularProgressIndicator.adaptive()),
    };
  }
  //TODO data export and import
}
