// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/error_content.dart';
import 'package:empado/providers/history_provider.dart';
import 'package:empado/screens/history/components/emotion_icon.dart';
import 'package:empado/screens/history/components/history_detail_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

class HistorySubscreen extends ConsumerWidget {
  const HistorySubscreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncFullSituations = ref.watch(fullSituationHistoryProvider);
    return
        //TODO allow exports
        switch (asyncFullSituations) {
      // TODO: Handle this case.
      AsyncData(value: final fullSituations) => CustomScrollView(slivers: [
          SliverAppBar(
            title: Text(AppLocalizations.of(context).history),
          ),
          SliverList.separated(
              itemCount: fullSituations.length,
              separatorBuilder: (context, _) => const Divider(),
              itemBuilder: (_, i) {
                final fullSituation = fullSituations[i];
                final formattedDate =
                    "${DateFormat.yMMMEd().format(fullSituation.situation.timestamp)} (${DateFormat.jm().format(fullSituation.situation.timestamp)})";
                return ListTile(
                  title: Text(fullSituation.situation.title != null
                      ? "${fullSituation.situation.title} | $formattedDate"
                      : formattedDate),
                  subtitle: Column(
                      //TODO differentiate between met and unmet needs
                      children: fullSituation.perspectives
                          .map((perspective) => Row(children: [
                                Expanded(
                                    child: Text((AppLocalizations.of(context)
                                            .personIntro(perspective
                                                    .person.name ??
                                                AppLocalizations.of(context)
                                                    .you)) + //TODO HAVE TO HAVE A SEPERATE INTRO IF MEETING NEEDS
                                        perspective.needs
                                            .map((n) =>
                                                n.need.getTranslation(context))
                                            .join(AppLocalizations.of(context)
                                                .listJoiner))),
                                const SizedBox(width: 8),
                                ...perspective.emotions.map((e) => EmotionIcon(
                                    perspectiveEmotion:
                                        e)), //TODO show first 3, then just have a counter that there's more
                              ]))
                          .toList()),
                  onTap: () async {
                    await HistoryDetailSheet.showSheet(context,
                        listSituation: fullSituation,
                        formattedDate: formattedDate, onDelete: () async {
                      await ref
                          .read(fullSituationHistoryProvider.notifier)
                          .deleteSituation(fullSituation.situation.id);
                      if (context.mounted) {
                        Navigator.of(context).pop();
                      }
                    });
                  },
                );
              })
        ]),
      AsyncError() => const Center(child: ErrorContent()),
      _ => const Center(child: CircularProgressIndicator.adaptive()),
    };
    // TODO allow deleting items
  }
}
