// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'dart:ui';

import 'package:empado/enums/theme_variant.dart';
import 'package:empado/models/setting_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _SettingStateNotifier extends AsyncNotifier<SettingState> {
  static const _localeKey = "locale";
  static const _themeKey = "theme";
  static const _systemColorsKey = "systemColors";

  static const _localeSeparator = "-";

  @override
  FutureOr<SettingState> build() {
    return _stateFromSharedPreferences();
  }

  Future<SettingState> _stateFromSharedPreferences() async {
    final preferences = await SharedPreferences.getInstance();
    final localePref =
        preferences.getString(_localeKey)?.split(_localeSeparator);
    final themePref = preferences.getInt(_themeKey);

    return SettingState(
        preferredLocale: (localePref != null)
            ? (localePref.length > 1)
                ? Locale(localePref[0], localePref[1])
                : Locale(localePref[0])
            : null,
        themeVariant:
            (themePref != null && ThemeVariant.values.length > themePref)
                ? ThemeVariant.values[themePref]
                : ThemeVariant.system,
        useSystemColors: preferences.getBool(_systemColorsKey) ?? false);
  }

  Future<void> setLocale(SettingState curState, Locale? locale) async {
    await _saveLocale(locale);

    state = AsyncValue.data(curState.copyWith(preferredLocale: locale));
  }

  Future<void> _saveLocale(Locale? locale) async {
    final preferences = await SharedPreferences.getInstance();
    if (locale != null) {
      if (locale.countryCode != null) {
        await preferences.setString(_localeKey,
            "${locale.languageCode}$_localeSeparator${locale.countryCode}");
      } else {
        await preferences.setString(_localeKey, locale.languageCode);
      }
    } else {
      await preferences.remove(_localeKey);
    }
  }

  Future<void> setThemeVariant(
      SettingState curState, ThemeVariant themeVariant) async {
    await _saveThemeVariant(themeVariant);

    state = AsyncValue.data(curState.copyWith(themeVariant: themeVariant));
  }

  Future<void> _saveThemeVariant(ThemeVariant themeVariant) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setInt(_themeKey, themeVariant.index);
  }

  Future<void> setUseSystemColors(
      SettingState curState, bool useSystemColors) async {
    await _saveUseSystemColors(useSystemColors);

    state =
        AsyncValue.data(curState.copyWith(useSystemColors: useSystemColors));
  }

  Future<void> _saveUseSystemColors(bool useSystemColors) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setBool(_systemColorsKey, useSystemColors);
  }
}

final settingProvider =
    AsyncNotifierProvider<_SettingStateNotifier, SettingState>(
        _SettingStateNotifier.new);
